import sys

class Tester(object):
    def __init__(self, landscape, bug):
        self.__landscape = self.read_file(landscape)
        self.__bug = self.read_file(bug)

    def count_bugs(self):
        if not self.__landscape or not self.__bug:
            return None

        #Limpia cadenas de espacios y saltos de linea
        landscape = "".join(self.__landscape.split())
        pattern_bug = "".join(self.__bug.split())


        lcad = []
        i = 0

        #Itera el texto principal buscando posible bug
        while i < len(landscape):
            if pattern_bug[0] == landscape[i]:
                subland = landscape[i:i+len(pattern_bug)]

                #Si encuentra bug, da un salto de indice
                if subland == pattern_bug:
                    lcad.append(subland)
                    i = i + len(pattern_bug) - 1
            i = i + 1

        print("Bugs", lcad)
        print("El numero total de bugs encontrados: ", len(lcad))


    #Lectura de archivo
    def read_file(self, file):
        try:
            with open(file, "r") as f:
                return f.read()

        except Exception as ex:
            print("Error en lectura de archivo, ", ex)
        return ""

if __name__ == "__main__":
    try:
        landscape = sys.argv[1]
        bug = sys.argv[2]
    except Exception as ex:
        print("Error en parametros de entrada")
        exit()

    ts = Tester(landscape, bug)
    ts.count_bugs()
